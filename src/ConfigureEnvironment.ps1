﻿param(
	[Parameter(Mandatory=$true)][String] $database_host, 
	[Parameter(Mandatory=$true)][String] $database_user,
	[Parameter(Mandatory=$true)][String] $database_password
)

[void][System.Reflection.Assembly]::LoadWithPartialName("MySql.Data")

try
{

	function Get-ConfigFilePath($fileName)
	{
		$hostFolderPath = (Get-Item $PSScriptRoot).FullName
		$path = Join-Path -Path $hostFolderPath -ChildPath "Albelli.CostumerApi.Host" | Join-Path -ChildPath 'appsettings.json'
		if (-not (Test-Path $path))
		{
			Write-Host "File '$path' does not exist" -BackgroundColor Red
			Exit
		}
		return $path 
	}	

	$baseFilePath = Get-ConfigFilePath "config.json"
	
	$logLevel = [pscustomobject]@{
		'Default'='Warning';
	}
	
	$logging = [pscustomobject]@{
		'logLevel'=$logLevel;
	}
	
	$connectionStrings = [pscustomobject]@{
		'DefaultConnection'="Server={0};Port=3306;Database=albelli;Uid={1};Pwd={2};" -f $database_host, $database_user, $database_password;
	}
	
	$config = [pscustomobject]@{
		'Logging'=$logging;
		'AllowedHosts'='*';
		'ConnectionStrings'=$connectionStrings 
	}

	$config | ConvertTo-Json -depth 100 | Out-File $baseFilePath  -Encoding utf8
	
	
	$connStr = "server=" + $database_host + ";port=3306;uid=" + $database_user + ";pwd=" + $database_password
	$conn = new-object MySql.Data.MySqlClient.MySqlConnection($connStr)
	$conn.Open();
	 
	# Create Database
    $createmysqldatabase = 'CREATE DATABASE `albelli` CHARACTER SET utf8'
    $cmd = New-Object MySql.Data.MySqlClient.MySqlCommand($createmysqldatabase, $conn)
    $cmd.ExecuteNonQuery()  

   
	Write-Host ("success")
}
catch
{
    Write-Error $_.Exception.ToString()
    Read-Host -Prompt "The above error occurred. Press Enter to exit."
}
