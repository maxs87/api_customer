﻿using Albelli.Database;
using System;

namespace Albelli.CostumerApi.Host.DTO
{
    public class OrderDTO : BaseModel
    {
        public DateTime CreatedDate { get; set; }
        public decimal Price { get; set; }
    }
}
