﻿using System.Collections.Generic;

namespace Albelli.CostumerApi.Host.DTO
{
    public class FullCustomerDTO : CustomerDTO
    {
        public IEnumerable<OrderDTO> Orders { get; set; }
    }
}
