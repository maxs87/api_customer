﻿using System.ComponentModel.DataAnnotations;

namespace Albelli.CostumerApi.Host.DTO
{
    public class CustomerDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        [EmailAddress]
        public string Email { get; set; }
    }
}
