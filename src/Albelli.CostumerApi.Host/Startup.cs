﻿using Albelli.Common.Middlewares.Exceptions;
using Albelli.Common.Swagger;
using Albelli.CostumerApi.Host.Mappings;
using Albelli.CostumerApi.Host.Repository;
using Albelli.CustomerApi.Migration;
using Albelli.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using Albelli.CostumerApi.Host.Engine;

namespace Albelli.CostumerApi.Host
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSwagger("Albelli Customer Api");

            services.AddAutoMapper(typeof(CustomerProfile), typeof(OrderProfile));
            services.AddDBConnection();
            services.AddSingleton(Configuration);

            services.AddTransient<IOderRepository, OderRepository>();
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<ICustomerEngine, CustomerEngine>();

            ///!!!!!!!!!! For simplicity !!!!!!!!!!!!!!!!!!!!!!!!
            ///Migrations sholud be used as separate component during CI/CD
            services.AddDatabaseMigration();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware(typeof(ErrorHandler));
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Albelli Customer Api V1");
            });

            app.UseCors(x =>
            {
                x.AllowAnyHeader();
                x.AllowAnyMethod();
                x.AllowAnyOrigin();
            });

            app.UseMvc();
        }
    }
}
