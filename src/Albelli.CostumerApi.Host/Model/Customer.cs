﻿using Albelli.Database;
using Dapper.Contrib.Extensions;

namespace Albelli.CostumerApi.Host.Model
{
    [Table("customers")]
    public class Customer : BaseModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
