﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albelli.CostumerApi.Host.Model
{
    public class FullCustomer : Customer
    {
        public IEnumerable<Order> Orders { get; set; }
    }
}
