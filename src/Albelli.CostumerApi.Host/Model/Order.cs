﻿using Albelli.Database;
using Dapper.Contrib.Extensions;
using System;

namespace Albelli.CostumerApi.Host.Model
{
    [Table("orders")]
    public class Order : BaseModel
    {
        public int CustomerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal Price { get; set; }
    }
}
