﻿using Albelli.CostumerApi.Host.DTO;
using Albelli.CostumerApi.Host.Model;
using AutoMapper;

namespace Albelli.CostumerApi.Host.Mappings
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            CreateMap<CustomerDTO, Customer>();
            CreateMap<Customer, CustomerDTO>();
            CreateMap<Customer, FullCustomer>();
            CreateMap<FullCustomer, FullCustomerDTO>();
        }
    }
}
