﻿using Albelli.CostumerApi.Host.DTO;
using Albelli.CostumerApi.Host.Model;
using AutoMapper;

namespace Albelli.CostumerApi.Host.Mappings
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<OrderDTO, Order>();
        }
    }
}
