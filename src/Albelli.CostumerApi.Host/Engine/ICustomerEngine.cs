﻿using Albelli.CostumerApi.Host.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Albelli.CostumerApi.Host.Engine
{
    public interface ICustomerEngine
    {
        Task<int> SaveCustomerAsync(Customer order);
        Task<IEnumerable<Customer>> GetCustomersAsync();
        Task<int> SaveOrderAsync(int customerId, Order order);
        Task<FullCustomer> GetCustomerAsync(int id);
        Task DeleteAsync(int customerId);
    }
}
