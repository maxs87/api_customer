﻿using Albelli.Common.Middlewares.Exceptions;
using Albelli.CostumerApi.Host.Model;
using Albelli.CostumerApi.Host.Repository;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Albelli.CostumerApi.Host.Engine
{
    public class CustomerEngine : ICustomerEngine
    {
        private readonly IOderRepository _orderRepostirory;
        private readonly ICustomerRepository _customerRepostirory;
        private readonly IMapper _mapper;

        public CustomerEngine(IOderRepository orderRepostirory,
            ICustomerRepository customerRepostirory,
            IMapper mapper)
        {
            _orderRepostirory = orderRepostirory;
            _customerRepostirory = customerRepostirory;
            _mapper = mapper;
        }

        public async Task<int> SaveCustomerAsync(Customer customer)
        {
            Customer result = await _customerRepostirory.GetCustomerByEmailAsync(customer.Email);
            if (result != null)
            {
                throw new ValueInvalidException($"Customer with email {customer.Email} already exist.");
            }
            return await _customerRepostirory.CreateAsync(customer);
        }

        public async Task<IEnumerable<Customer>> GetCustomersAsync()
        {
            return await _customerRepostirory.GetAsync();
        }

        public async Task<FullCustomer> GetCustomerAsync(int id)
        {
            Customer customer = await _customerRepostirory.GetAsync(id);
            if (customer == null)
            {
                throw new RecordsNotFoundException($"Customer with id {id} not found");
            }
            FullCustomer result = _mapper.Map<FullCustomer>(customer);

            IEnumerable<Order> orders = await _orderRepostirory.GetOrdersForCustomerAsync(id);
            result.Orders = orders;
            return result;
        }

        public async Task<int> SaveOrderAsync(int customerId, Order order)
        {
            Customer customer = await _customerRepostirory.GetAsync(customerId);
            if (customer == null)
            {
                throw new ValueInvalidException($"Customer with id {customerId} is invalid");
            }
            order.CustomerId = customerId;
            order.CreatedDate = DateTime.UtcNow;
            return await _orderRepostirory.CreateAsync(order);
        }

        public async Task DeleteAsync(int customerId)
        {
            Customer customer = await _customerRepostirory.GetAsync(customerId);
            if (customer == null)
            {
                throw new ValueInvalidException($"Customer with id {customerId} not found");
            }
            await _customerRepostirory.DeleteAsync(customerId);
        }
    }
}
