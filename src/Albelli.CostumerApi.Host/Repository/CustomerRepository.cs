﻿using Albelli.CostumerApi.Host.Model;
using Albelli.Database;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace Albelli.CostumerApi.Host.Repository
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<Customer> GetCustomerByEmailAsync(string email);
    }

    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }

        public override async Task DeleteAsync(int customerId)
        {
            using (var transactionScope = new TransactionScope())
            {
                await QueryAsync<Order>("delete from orders where CustomerId = @CustomerId", new { CustomerId = customerId });
                await QueryAsync<Customer>("delete from customers where Id = @CustomerId", new { CustomerId = customerId });
                transactionScope.Complete();
            }
        }

        public async Task<Customer> GetCustomerByEmailAsync(string email)
        {
            var result = await QueryAsync<Customer>("select * from customers where Email = @Email", new { Email = email });
            return result.FirstOrDefault();
        }

    }
}
