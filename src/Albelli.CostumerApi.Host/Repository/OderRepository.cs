﻿using Albelli.CostumerApi.Host.Model;
using Albelli.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albelli.CostumerApi.Host.Repository
{
    public interface IOderRepository : IRepository<Order>
    {
        Task<IEnumerable<Order>> GetOrdersForCustomerAsync(int customerId);
    }

    public class OderRepository : BaseRepository<Order>, IOderRepository
    {
        public OderRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }

        public async Task<IEnumerable<Order>> GetOrdersForCustomerAsync(int customerId)
        {
            const string query = @"select * from orders where CustomerId = @CustomerId";
            return await QueryAsync<Order>(query, new { CustomerId = customerId });
        }

    }
}
