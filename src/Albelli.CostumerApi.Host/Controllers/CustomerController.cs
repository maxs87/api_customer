﻿using Albelli.CostumerApi.Host.DTO;
using Albelli.CostumerApi.Host.Engine;
using Albelli.CostumerApi.Host.Model;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Albelli.CostumerApi.Host.Controllers
{
    [Route("api/v1/customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerEngine _customerEngine;
        private readonly IMapper _mapper;
        public CustomerController(ICustomerEngine customerEngine,
            IMapper mapper)
        {
            _customerEngine = customerEngine;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CustomerDTO request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customer = _mapper.Map<Customer>(request);
            int result = await _customerEngine.SaveCustomerAsync(customer);
            return Ok(new { createdId = result });
        }

        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(List<CustomerDTO>))]
        public async Task<IActionResult> GetCustomersAsync()
        {
            var customers = await _customerEngine.GetCustomersAsync();
            return Ok(_mapper.Map<List<CustomerDTO>>(customers));
        }

        [HttpGet("{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(FullCustomerDTO))]
        public async Task<IActionResult> GetCustomerAsync(int id)
        {
            var customer = await _customerEngine.GetCustomerAsync(id);
            return Ok(_mapper.Map<FullCustomerDTO>(customer));
        }

        [HttpPost("{id}/orders")]
        public async Task<IActionResult> CreateOrderAsync(int id, [FromBody] OrderDTO request)
        {
            var order = _mapper.Map<Order>(request);
            int result = await _customerEngine.SaveOrderAsync(id, order);
            return Ok(new { createdId = result });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomerAsync(int id)
        {
            await _customerEngine.DeleteAsync(id);
            return Ok();
        }

    }
}
