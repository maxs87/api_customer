﻿using Albelli.Database;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;


namespace Albelli.CustomerApi.Migration
{
    public static class DatabaseMigrationServiceConfiguration
    {
        public static void AddDatabaseMigration(this IServiceCollection services)
        {
            IConnectionFactory connectionFactory = services.BuildServiceProvider().GetRequiredService<IConnectionFactory>();

            services.AddLogging(lb => lb.AddFluentMigratorConsole())
               .AddFluentMigratorCore()
               .ConfigureRunner(
                   builder => builder
                   .AddMySql5()
                   .WithGlobalConnectionString(connectionFactory.GetConnection().ConnectionString)
                   .WithMigrationsIn(typeof(DatabaseMigrationServiceConfiguration).Assembly)
            );

            IMigrationRunner runner = services.BuildServiceProvider().GetRequiredService<IMigrationRunner>();
            runner.MigrateUp();
        }
    }
}
