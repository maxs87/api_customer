﻿using FluentMigrator;

namespace Albelli.CustomerApi.Migration._2018._06
{
    [Migration(201806101100)]
    public class CreateOrderTable : MigrationBase
    {
        public override void Up()
        {
            Create.Table("Orders")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("Price").AsDecimal()
                .WithColumn("CustomerId").AsInt32().ForeignKey("Customers", "Id")
                .WithColumn("CreatedDate").AsDateTime();
        }
        public override void Down()
        {
            Create.Table("Order");
        }
    }
}
