﻿using FluentMigrator;

namespace Albelli.CustomerApi.Migration._2018._06
{
    [Migration(201806091500)]
    public class CreateCustomerTable : MigrationBase
    {
        public override void Up()
        {
            Create.Table("Customers")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("Name").AsString(32)
                .WithColumn("Email").AsString().Unique();
        }
        public override void Down()
        {
            Create.Table("Customer");
        }
    }
}
