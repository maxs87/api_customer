CREATE DATABASE `albelli` CHARACTER SET UTF8MB4;

USE albelli;

CREATE TABLE `customers` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Name` VARCHAR(32) NOT NULL COLLATE 'utf8_general_ci',
	`Email` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	PRIMARY KEY (`Id`),
	UNIQUE INDEX `IX_Customers_Email` (`Email`)
)COLLATE='utf8mb4_0900_ai_ci'
ENGINE=InnoDB;


CREATE TABLE `orders` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Price` DECIMAL(19,5) NOT NULL,
	`CustomerId` INT(11) NOT NULL,
	`CreatedDate` DATETIME NOT NULL,
	PRIMARY KEY (`Id`),
	INDEX `FK_Orders_CustomerId_Customers_Id` (`CustomerId`),
	CONSTRAINT `FK_Orders_CustomerId_Customers_Id` FOREIGN KEY (`CustomerId`) REFERENCES `customers` (`id`)
)COLLATE='utf8mb4_0900_ai_ci'
ENGINE=InnoDB;

CREATE TABLE `versioninfo` (
	`Version` BIGINT(20) NOT NULL,
	`AppliedOn` DATETIME NULL DEFAULT NULL,
	`Description` VARCHAR(1024) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	UNIQUE INDEX `UC_Version` (`Version`)
)
COLLATE='utf8mb4_0900_ai_ci'
ENGINE=InnoDB;

INSERT INTO `versioninfo` (`Version`, `AppliedOn`, `Description`) VALUES (201806091500, '2018-06-10 12:00:13', 'CreateCustomerTable');
INSERT INTO `versioninfo` (`Version`, `AppliedOn`, `Description`) VALUES (201806101100, '2018-06-10 12:00:13', 'CreateOrderTable');
