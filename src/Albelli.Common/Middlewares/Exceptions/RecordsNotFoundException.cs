﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;

namespace Albelli.Common.Middlewares.Exceptions
{
    public class RecordsNotFoundException : Exception, IServiceException
    {
        public RecordsNotFoundException(string message) : base(message)
        {
        }

        public void ModifyHttpResponse(HttpResponse response, out string content)
        {
            content = JsonConvert.SerializeObject(new { error = Message });
            response.StatusCode = (int)HttpStatusCode.NotFound;
        }
    }
}
