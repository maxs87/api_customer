﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Albelli.Common.Middlewares.Exceptions
{
    public class ErrorHandler
    {
        private readonly RequestDelegate next;

        public ErrorHandler(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            IServiceException serviceException = exception as IServiceException;
            string content = "";
            if (serviceException != null)
            {
                serviceException.ModifyHttpResponse(context.Response, out content);
            }
            else
            {
                content = JsonConvert.SerializeObject(new { error = "Service unailable" });
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            context.Response.ContentType = "application/json";
            return context.Response.WriteAsync(content);
        }
    }
}
