﻿using Microsoft.AspNetCore.Http;

namespace Albelli.Common.Middlewares.Exceptions
{
    public interface IServiceException
    {
        void ModifyHttpResponse(HttpResponse response, out string content);
    }
}
