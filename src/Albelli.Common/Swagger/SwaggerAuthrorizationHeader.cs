﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace Albelli.Common.Swagger
{
    public class SwaggerAuthrorizationHeader : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();

            var actionDescriptor = context.ApiDescription.ActionDescriptor;

            if (!actionDescriptor.HasAttribute<Microsoft.AspNetCore.Authorization.AllowAnonymousAttribute>())
            {
                operation.Parameters.Add(new NonBodyParameter()
                {
                    Name = "Authorization",
                    In = "header",
                    Type = "string",
                    Required = false,
                    Description = "The authorization token"
                });
            }
        }
    }
}
