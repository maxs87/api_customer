﻿using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;

namespace Albelli.Common.Swagger
{
    public static class SwaggerServiceConfiguration
    {
        public static void AddSwagger(this IServiceCollection services, string componentName)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = componentName, Version = "v1" });
                c.OperationFilter<SwaggerAuthrorizationHeader>();
            });
        }

        public static bool HasAttribute<T>(this ActionDescriptor actionDescriptor) where T : Attribute
        {
            var ccontrollerActionDescriptor = actionDescriptor as ControllerActionDescriptor;
            foreach (var attribute in ccontrollerActionDescriptor.MethodInfo.GetCustomAttributes(inherit: true))
            {
                if (attribute.GetType() == typeof(T))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
