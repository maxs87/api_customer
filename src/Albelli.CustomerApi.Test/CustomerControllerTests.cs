﻿using Albelli.CostumerApi.Host.Controllers;
using Albelli.CostumerApi.Host.DTO;
using Albelli.CostumerApi.Host.Engine;
using Albelli.CostumerApi.Host.Model;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albelli.CustomerApi.Test
{
    public class CustomerControllerTests
    {
        private readonly Mock<ICustomerEngine> _customerEngineMock = new Mock<ICustomerEngine>();
        private readonly CustomerController _customerController;
        private readonly Mock<IMapper> _mapperMock;

        public CustomerControllerTests()
        {
            var mockRepository = new MockRepository(MockBehavior.Loose);
            _mapperMock = mockRepository.Create<IMapper>();
            _customerController = new CustomerController(_customerEngineMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task CreateAsync_CallsEngineWithCorrectArgument()
        {
            var expectedDto = new CustomerDTO() { Email = "test@fdsftest.fdsf" };
            var expectedModel = new Customer() { Email = "test@fdsftest.fdsf" };
            _customerEngineMock.Setup(x => x.SaveCustomerAsync(It.IsAny<Customer>())).ReturnsAsync(3);
            _mapperMock.Setup(x => x.Map<Customer>(It.IsAny<CustomerDTO>())).Returns(expectedModel);

            var actualResult = await _customerController.CreateAsync(expectedDto) as OkObjectResult;

            _customerEngineMock.Verify(x => x.SaveCustomerAsync(expectedModel), Times.Once);
            _mapperMock.Verify(x => x.Map<Customer>(expectedDto), Times.Once);
            actualResult.Value.Should().BeEquivalentTo(new { createdId = 3 });
        }


        [Fact]
        public async Task CreateAsync_ReturnsBadRequestIfIsInvalid()
        {
            var expectedCustomer = new CustomerDTO() { };
            _customerController.ModelState.AddModelError("Email", "error");

            var actualResult = await _customerController.CreateAsync(expectedCustomer) as BadRequestObjectResult;

            actualResult.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task GetCustomersAsync_ReturnsCustomersCollection()
        {
            var expectedModel = new List<Customer>();

            var expectedResult = new List<CustomerDTO>();
            expectedResult.Add(new CustomerDTO { Email = "123123", Name = "3213" });

            _customerEngineMock.Setup(x => x.GetCustomersAsync()).ReturnsAsync(expectedModel);
            _mapperMock.Setup(x => x.Map<List<CustomerDTO>>(It.IsAny<List<Customer>>())).Returns(expectedResult);

            var actualResult = await _customerController.GetCustomersAsync() as OkObjectResult;

            _customerEngineMock.Verify(x => x.GetCustomersAsync(), Times.Once);
            actualResult.Value.Should().BeEquivalentTo(expectedResult);
        }

        [Fact]
        public async Task GetCustomerAsync_ReturnsCustomerWithOrders()
        {
            var expectedModel = new FullCustomer();

            var expectedResult = new FullCustomerDTO() { Email = "email@fdsf", Orders = new List<OrderDTO>() { new OrderDTO { Id = 1 } } };
            int customerId = 2;
            _customerEngineMock.Setup(x => x.GetCustomerAsync(customerId)).ReturnsAsync(expectedModel);
            _mapperMock.Setup(x => x.Map<FullCustomerDTO>(It.IsAny<FullCustomer>())).Returns(expectedResult);

            var actualResult = await _customerController.GetCustomerAsync(2) as OkObjectResult;

            _mapperMock.Verify(x => x.Map<FullCustomerDTO>(expectedModel), Times.Once);
            _customerEngineMock.Verify(x => x.GetCustomerAsync(customerId), Times.Once);
            actualResult.Value.Should().BeEquivalentTo(expectedResult);
        }

        [Fact]
        public async Task CreateOrderAsync_CallsEngineWithCorrectArgument()
        {
            var order = new OrderDTO() { Price = 4 };
            var expectedOrder = new Order();
            int customerId = 5;
            _customerEngineMock.Setup(x => x.SaveOrderAsync(It.IsAny<int>(), It.IsAny<Order>())).ReturnsAsync(3);
            _mapperMock.Setup(x => x.Map<Order>(It.IsAny<OrderDTO>())).Returns(expectedOrder);

            var actualResult = await _customerController.CreateOrderAsync(customerId, order) as OkObjectResult;

            _customerEngineMock.Verify(x => x.SaveOrderAsync(5, expectedOrder), Times.Once);
            actualResult.Value.Should().BeEquivalentTo(new { createdId = 3 });
        }

        [Fact]
        public async Task DeleteCustomerAsync_CallsEngineWithCorrectArgument()
        {
            _customerEngineMock.Setup(x => x.DeleteAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var actualResult = await _customerController.DeleteCustomerAsync(3) as OkResult;

            _customerEngineMock.Verify(x => x.DeleteAsync(3), Times.Once);
            actualResult.StatusCode.Should().Be(200);
        }

    }
}
