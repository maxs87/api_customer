﻿using Albelli.Common.Middlewares.Exceptions;
using Albelli.CostumerApi.Host.Engine;
using Albelli.CostumerApi.Host.Model;
using Albelli.CostumerApi.Host.Repository;
using AutoMapper;
using FluentAssertions;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Albelli.CustomerApi.Test
{
    public class CustomerEngineTests
    {

        private readonly ICustomerEngine _customerEngine;

        private readonly Mock<ICustomerRepository> _customerRepositoryMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IOderRepository> _orderRepositoryMock;

        public CustomerEngineTests()
        {
            var mockRepository = new MockRepository(MockBehavior.Loose);

            _customerRepositoryMock = mockRepository.Create<ICustomerRepository>();
            _orderRepositoryMock = mockRepository.Create<IOderRepository>();
            _mapperMock = mockRepository.Create<IMapper>();
            _customerEngine = new CustomerEngine(_orderRepositoryMock.Object, _customerRepositoryMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task SaveCustomerAsync_ThrowsExceptionIfEmailNotUnique()
        {
            var existedCustomer = new Customer() { };
            _customerRepositoryMock.Setup(x => x.GetCustomerByEmailAsync(It.IsAny<string>())).ReturnsAsync(existedCustomer);

            await Assert.ThrowsAsync<ValueInvalidException>(() => _customerEngine.SaveCustomerAsync(existedCustomer));
        }

        [Fact]
        public async Task SaveCustomerAsync_CallsRepositoryWithCorrectArguments()
        {
            var customer = new Customer() { Email = "test1" };
            Customer existedCustomer = null;
            _customerRepositoryMock.Setup(x => x.GetCustomerByEmailAsync(It.IsAny<string>())).ReturnsAsync(existedCustomer);
            _customerRepositoryMock.Setup(x => x.CreateAsync(It.IsAny<Customer>()))
                .ReturnsAsync(3)
                .Callback<Customer>(r => r.Should().BeEquivalentTo(new Customer() { Email = "test1" }));

            int result = await _customerEngine.SaveCustomerAsync(customer);

            result.Should().Be(3);
            _customerRepositoryMock.Verify(x => x.CreateAsync(customer), Times.Once);
        }

        [Fact]
        public async Task GetCustomersAsync_ReturnsCorrectCollection()
        {
            var returnedResult = new List<Customer>();
            returnedResult.Add(new Customer { Email = "123123", Name = "3213" });

            //create copy to check that value didnt changed later
            var expectedResult = new List<Customer>();
            expectedResult.Add(new Customer { Email = "123123", Name = "3213" });

            _customerRepositoryMock.Setup(x => x.GetAsync()).ReturnsAsync(returnedResult);

            IEnumerable<Customer> actualResult = await _customerEngine.GetCustomersAsync();

            _customerRepositoryMock.Verify(x => x.GetAsync(), Times.Once);
            actualResult.Should().BeEquivalentTo(returnedResult);
        }

        [Fact]
        public async Task GetCustomerAsync_ThrowsExceptionIfNoCustomerFound()
        {
            Customer customer = null;
            _customerRepositoryMock.Setup(x => x.GetAsync(It.IsAny<int>())).ReturnsAsync(customer);

            await Assert.ThrowsAsync<RecordsNotFoundException>(() => _customerEngine.GetCustomerAsync(1));
        }

        [Fact]
        public async Task GetCustomerAsync_ReturnsCorrectCustomer()
        {
            List<Order> orders = new List<Order>() { new Order() };
            FullCustomer expectedCustomer = new FullCustomer() { Id = 1, Email = "test", Orders = orders };
            FullCustomer returnedCustomer = new FullCustomer() { Id = 1, Email = "test" };
            int customerId = 3;
            _customerRepositoryMock.Setup(x => x.GetAsync(customerId)).ReturnsAsync(new Customer() { Id = 3 });
            _mapperMock.Setup(x => x.Map<FullCustomer>(It.IsAny<Customer>())).Returns(returnedCustomer);
            _orderRepositoryMock.Setup(x => x.GetOrdersForCustomerAsync(customerId)).ReturnsAsync(orders);

            var actualResult = await _customerEngine.GetCustomerAsync(customerId);

            _customerRepositoryMock.Verify(x => x.GetAsync(customerId), Times.Once);
            _orderRepositoryMock.Verify(x => x.GetOrdersForCustomerAsync(customerId), Times.Once);
            expectedCustomer.Should().BeEquivalentTo(actualResult);
        }

        [Fact]
        public async Task SaveOrderAsync_CallsRepositoryWithCorrectArgument()
        {
            int customerId = 3;
            var order = new Order() { Price = 3 };
            Customer existedCustomer = new Customer() { Id = 3 };

            _customerRepositoryMock.Setup(x => x.GetAsync(customerId)).ReturnsAsync(existedCustomer);
            _orderRepositoryMock.Setup(x => x.CreateAsync(It.IsAny<Order>()))
                .ReturnsAsync(2);

            var actualResult = await _customerEngine.SaveOrderAsync(customerId, order);

            _orderRepositoryMock.Verify(x => x.CreateAsync(order), Times.Once);
            actualResult.Should().Be(2);
        }

        [Fact]
        public async Task SaveOrderAsync_ThrowsExceptionIfNoCustomerFound()
        {
            Customer customer = null;
            _customerRepositoryMock.Setup(x => x.GetAsync(It.IsAny<int>())).ReturnsAsync(customer);

            await Assert.ThrowsAsync<ValueInvalidException>(() => _customerEngine.SaveOrderAsync(It.IsAny<int>(), It.IsAny<Order>()));
        }

        [Fact]
        public async Task DeleteAsync_ThrowsExceptionIfNoCustomerFound()
        {
            Customer customer = null;
            _customerRepositoryMock.Setup(x => x.GetAsync(It.IsAny<int>())).ReturnsAsync(customer);
            await Assert.ThrowsAsync<ValueInvalidException>(() => _customerEngine.DeleteAsync(It.IsAny<int>()));
        }

        [Fact]
        public async Task DeleteAsync_CallsRepositoryWithCorrectArguments()
        {
            int customerId = 2;
            Customer existedCustomer = new Customer() { Id = 3 };
            _customerRepositoryMock.Setup(x => x.GetAsync(It.IsAny<int>())).ReturnsAsync(existedCustomer);
            _customerRepositoryMock.Setup(x => x.DeleteAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            await _customerEngine.DeleteAsync(customerId);

            _customerRepositoryMock.Verify(x => x.DeleteAsync(customerId), Times.Once);
        }

    }
}
