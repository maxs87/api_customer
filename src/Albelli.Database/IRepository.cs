﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Albelli.Database
{
    public interface IRepository<T> where T : class
    {
        Task<int> CreateAsync(T entity);
        Task<T> GetAsync(int id);
        Task<IEnumerable<T>> GetAsync();
        Task DeleteAsync(int id);
    }
}
