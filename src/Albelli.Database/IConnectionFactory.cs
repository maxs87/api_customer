﻿using System.Data;

namespace Albelli.Database
{
    public interface IConnectionFactory
    {
        IDbConnection GetConnection();
    }
}
