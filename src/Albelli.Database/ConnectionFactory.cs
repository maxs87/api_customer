﻿using System.Configuration;
using System.Data;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace Albelli.Database
{
    public class ConnectionFactory : IConnectionFactory
    {
        private readonly string _connectionString;

        private const string ConnectionString = "DefaultConnection";

        public ConnectionFactory(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString(ConnectionString);
            if (_connectionString == null)
            {
                throw new ConfigurationErrorsException($"Connection string with the name of '{ConnectionString}' was not found.");
            }
        }

        public IDbConnection GetConnection()
        {
            return new MySqlConnection(_connectionString);
        }

    }
}
