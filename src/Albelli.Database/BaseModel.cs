﻿namespace Albelli.Database
{
    public abstract class BaseModel
    {
        public int Id { get; set; }
    }
}
