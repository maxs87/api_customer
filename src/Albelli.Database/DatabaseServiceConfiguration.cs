﻿using Microsoft.Extensions.DependencyInjection;

namespace Albelli.Database
{
    public static class DatabaseServiceConfiguration
    {
        public static void AddDBConnection(this IServiceCollection services)
        {
            services.AddTransient<IConnectionFactory, ConnectionFactory>();
        }
    }
}
