﻿using Dapper.Contrib.Extensions;
using System;
using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using Dapper;

namespace Albelli.Database
{
    public class BaseRepository
    {
        private readonly IConnectionFactory _connectionFactory;

        public BaseRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string query, object model = null)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                return await connection.QueryAsync<T>(query, model);
            }
        }

        public async Task<int> InsertAsync<T>(T entity) where T : BaseModel
        {
            return await ExecuteAsync(connection => connection.InsertAsync(entity));
        }

        public async Task DeleteAsync<T>(int id) where T : BaseModel, new()
        {
            await ExecuteAsync(async connection => await connection.DeleteAsync(new T { Id = id }));
        }

        public async Task<T> GetAsync<T>(int id) where T : BaseModel, new()
        {
            return await ExecuteAsync(async connection => await connection.GetAsync<T>(id));
        }

        public async Task<IEnumerable<T>> GetAsync<T>() where T : BaseModel, new()
        {
            return await ExecuteAsync(async connection => await connection.GetAllAsync<T>());
        }

        private IDbConnection GetConnection()
        {
            return _connectionFactory.GetConnection();
        }

        private async Task<TR> ExecuteAsync<TR>(Func<IDbConnection, Task<TR>> func)
        {
            using (var connection = GetConnection())
            {
                return await func.Invoke(connection);
            }
        }

    }


    public abstract class BaseRepository<T> : BaseRepository, IRepository<T> where T : BaseModel, new()
    {
        protected BaseRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }

        public virtual async Task<int> CreateAsync(T entity) => await InsertAsync(entity);
        public virtual async Task DeleteAsync(int id) => await DeleteAsync<T>(id);
        public virtual async Task<T> GetAsync(int id) => await GetAsync<T>(id);
        public virtual async Task<IEnumerable<T>> GetAsync() => await GetAsync<T>();
    }
}
