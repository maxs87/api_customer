Max's Shepel Technical Assignment, .NET Software Engineer for Albelli.

### Technologies and tools used for development:

ASP.NET Core (Web Api) 2.1.0,
Swagger 2.5.0,
MySQL,
Dapper 1.50.5,
Fluent Migrator 3.1.0,
AutoMapper 7.0.0,
xUnit 2.3.1,
Fluent Assertions 5.3.2,
PowerShell,

### Application was **tested** on following systems:

         Operating System: Windows 10 Pro 64-bit (10.0, Build 17134)
      System Manufacturer: Gigabyte Technology Co., Ltd.
                Processor: Intel(R) Core(TM) i7-6700 CPU @ 3.40GHz (8 CPUs), ~3.4GHz
                   Memory: 32768MB RAM
------------------------
      MacBook Pro
      OS: MacOS Sierra 10.12  
      Model Identifier:	MacBookPro8,1
      Processor Name:	Intel Core i5
      Processor Speed:	2.4 GHz


### How to run:
Prepare the environment:
	run powershell script "ConfigureEnvironment.ps1". It will create database (mysql sholud be installed with mysql adapter in GAC) and set the connection string 
Or
run the sql script migraion.sql and set the connection string manualy

Run the application:

```sh
cd Albelli.CostumerApi.Host
$ dotnet restore
$ dotnet run
```
or run with Visual Studio. All migrations will run(if necessary) and application will be available on **localhost:{port}/swagger**

### Simplifications

Following simplifications were made during the developement:

-	 Project Albelli.Common
		Is a part of web service solution, on production should be a separate nuget package in order to provide general features (like swagger, or error handling) to other services or microservices

-	Project Albelli.Database
		Is a part of web service solution, on production should be a separate nuget package in order to provide access to database for other services or microservices

-	Project Albelli.CustomerApi.Migration
		Is a part of web service solution and migrations run during application start process. On production it can be a separate project to run migrations during CI/CD
	
### Further improvements
Following features can be added:

- Paging, sorting, filtering for customers

- Error logging,

- Environments support (dev, qa, prod, etc)

- Also in systems operating with custumer's and orders, orders are usualy a core entity of the system and can be handled by different components so it is reasonable to use some notification system like Amazon SNS/SQS. It will allow to publih new order to multiple handlers and preserve the order message if one or more handlers is unavailable for some reasons